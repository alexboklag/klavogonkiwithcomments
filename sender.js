const comments = require('./comments.json');

//Facade
class SenderOfComments {
    constructor({ socket, userName, secondsToFinishGame, metersToFinishGame }) {
        this.socket = socket;
        this.userName = userName;
        this.secondsToFinishGame = secondsToFinishGame;
        this.metersToFinishGame = metersToFinishGame;
    }
    sendComment() {
        new Greeting({
            socket: this.socket,
            userName: this.userName,
            secondsToFinishGame: this.secondsToFinishGame
        });
        new AnnounceCompetitors({
            socket: this.socket,
            userName: this.userName,
            secondsToFinishGame: this.secondsToFinishGame
        });
        new Joke1({
            socket: this.socket,
            userName: this.userName,
            secondsToFinishGame: this.secondsToFinishGame
        });
        new PeriodicMessage({
            socket: this.socket,
            userName: this.userName,
            secondsToFinishGame: this.secondsToFinishGame
        });
        new Joke2({
            socket: this.socket,
            userName: this.userName,
            secondsToFinishGame: this.secondsToFinishGame
        });
        new FewMetersToFinish({
            socket: this.socket,
            userName: this.userName,
            metersToFinishGame: this.metersToFinishGame
        });
        new AnnounceFinisher({
            socket: this.socket,
            userName: this.userName,
            metersToFinishGame: this.metersToFinishGame
        });
        new AnnounceResults({
            socket: this.socket,
            userName: this.userName,
            secondsToFinishGame: this.secondsToFinishGame
        });
    }
}
class Greeting {
    constructor({ socket, userName, secondsToFinishGame }) {
        if (secondsToFinishGame < 60 && secondsToFinishGame > 50) {
            socket.emit('addComment', { userName, comment: comments.greeting });
        }
    }
}
class AnnounceCompetitors {
    constructor({ socket, userName, secondsToFinishGame }) {
        if (secondsToFinishGame < 50 && secondsToFinishGame > 45) {
            socket.emit('addComment', { userName, comment: comments.announceCompetitors });
        }
    }
}
class Joke1 {
    constructor({ socket, userName, secondsToFinishGame }) {
        if (secondsToFinishGame < 45 && secondsToFinishGame > 35) {
            socket.emit('addComment', { userName, comment: comments.joke1 });
        }
    }
}
class PeriodicMessage {
    constructor({ socket, userName, secondsToFinishGame }) {
        if ((secondsToFinishGame % 30 == 0) && (secondsToFinishGame != 60) && (secondsToFinishGame != 0)) {
            socket.emit('addComment', { userName, comment: comments.periodicMessage });
        }
    }
}
class Joke2 {
    constructor({ socket, userName, secondsToFinishGame }) {
        if (secondsToFinishGame < 20) {
            socket.emit('addComment', { userName, comment: comments.joke2 });
        }
    }
}
class FewMetersToFinish {
    constructor({ socket, userName, metersToFinishGame }) {
        if (metersToFinishGame == 30) {
            socket.emit('addComment', { userName, comment: comments.fewMetersToFinish });
        }
    }
}
class AnnounceFinisher {
    constructor({ socket, userName, metersToFinishGame }) {
        if (metersToFinishGame == 0) {
            socket.emit('addComment', { userName, comment: comments.announceFinisher });
        }
    }
}
class AnnounceResults {
    constructor({ socket, userName, secondsToFinishGame }) {
        if (secondsToFinishGame == 0) {
            socket.emit('announceResults', { userName, comment: comments.announceResults });
        }
    }
}