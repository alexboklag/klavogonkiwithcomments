const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const texts = require('./texts.json');
const comments = require('./comments.json');
require('./passport.config');
require('./sender');

let n = 2;
const currentGame = {
  timeStart: false,
  roomId: 28,
  text: texts[n % 3],
  users: {}
};

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/klavogonki', (req, res) => {
  res.sendFile(path.join(__dirname, 'klavogonki.html'));
});

app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.post('/login', (req, res) => {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret');
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

io.on('connection', socket => {
  if (!currentGame.timeStart) {
    const timeStart = new Date();
    timeStart.setSeconds(timeStart.getSeconds() + 20);
    currentGame.timeStart = timeStart;
  }
  socket.emit('currentGame', currentGame);

  socket.on('join', ({ roomId }) => {
    socket.join(roomId);
  });

  socket.on('currentGameResults', ({ secondsToFinishGame, curResult, maxResult, token }) => {
    const user = jwt.verify(token, 'someSecret');

    if (user) {
      const userLogin = jwt.decode(token).login;
      const userName = jwt.decode(token).name;

      currentGame.users[`${userLogin}`] = {
        name: userName,
        curResult,
        maxResult
      };
      const metersToFinishGame = maxResult - curResult;

      //Proxy
      let SenderOfCommentsProxy = new Proxy(SenderOfComments, {
        construct: (target, args) => {
          console.log('SenderOfComments constructor called!');
          return new target(...args);
        }
      });

      const sender = new SenderOfCommentsProxy({ socket, userName, secondsToFinishGame, metersToFinishGame });
      sender.tryToSendComment('greeting');
      sender.tryToSendComment('announceCompetitors');
      sender.tryToSendComment('joke1');
      sender.tryToSendComment('periodicMessage');
      sender.tryToSendComment('joke2');
      sender.tryToSendComment('fewMetersToFinish');
      sender.tryToSendComment('announceFinisher');
      sender.tryToSendComment('announceResults');
    }
  })

  socket.on('submitResults', () => {
    socket.emit('allResults', currentGame);
    socket.broadcast.to(currentGame.roomId).emit('allResults', currentGame);
  });

  socket.on('disconnect', () => {
    socket.broadcast.emit('somebodyDisconnected');
  });

});

//Facade
class SenderOfComments {
  constructor(info) {
    this.factory = new Factory(info);
  }
  tryToSendComment(type) {
    this.factory.createComment(type);
  }
}

//Factory
class Factory {
  constructor(info) {
    this.info = info;
  }
  createComment(type) {
    if (type == 'greeting') {
      new Greeting(this.info);
    }
    else if (type == 'announceCompetitors') {
      new AnnounceCompetitors(this.info);
    }
    else if (type == 'joke1') {
      new Joke1(this.info);
    }
    else if (type == 'periodicMessage') {
      new PeriodicMessage(this.info);
    }
    else if (type == 'joke2') {
      new Joke2(this.info);
    }
    else if (type == 'fewMetersToFinish') {
      new FewMetersToFinish(this.info);
    }
    else if (type == 'announceFinisher') {
      new AnnounceFinisher(this.info);
    }
    else if (type == 'announceResults') {
      new AnnounceResults(this.info);
    }
  }
}

class Greeting {
  constructor({ socket, userName, secondsToFinishGame }) {
    if (secondsToFinishGame < 60 && secondsToFinishGame > 50) {
      socket.emit('addComment', { userName, comment: comments.greeting });
    }
  }
}
class AnnounceCompetitors {
  constructor({ socket, userName, secondsToFinishGame }) {
    if (secondsToFinishGame < 50 && secondsToFinishGame > 40) {
      socket.emit('addComment', { userName, comment: comments.announceCompetitors });
    }
  }
}
class Joke1 {
  constructor({ socket, userName, secondsToFinishGame }) {
    if (secondsToFinishGame < 40 && secondsToFinishGame > 30) {
      socket.emit('addComment', { userName, comment: comments.joke1 });
    }
  }
}
class PeriodicMessage {
  constructor({ socket, userName, secondsToFinishGame }) {
    if ((secondsToFinishGame % 30 == 0) && (secondsToFinishGame != 60) && (secondsToFinishGame != 0)) {
      socket.emit('addComment', { userName, comment: comments.periodicMessage });
    }
  }
}
class Joke2 {
  constructor({ socket, userName, secondsToFinishGame }) {
    if (secondsToFinishGame < 20) {
      socket.emit('addComment', { userName, comment: comments.joke2 });
    }
  }
}
class FewMetersToFinish {
  constructor({ socket, userName, metersToFinishGame }) {
    if (metersToFinishGame == 30) {
      socket.emit('addComment', { userName, comment: comments.fewMetersToFinish });
      socket.broadcast.to(currentGame.roomId).emit('addComment', { userName, comment: comments.announceFinisher });
    }
  }
}
class AnnounceFinisher {
  constructor({ socket, userName, metersToFinishGame }) {
    if (metersToFinishGame == 0) {
      socket.emit('addComment', { userName, comment: comments.announceFinisher });
      socket.broadcast.to(currentGame.roomId).emit('addComment', { userName, comment: comments.announceFinisher });
    }
  }
}
class AnnounceResults {
  constructor({ socket, userName, secondsToFinishGame }) {
    if (secondsToFinishGame < 1) {
      socket.emit('addComment', { userName, comment: comments.announceResults });
    }
  }
}